//
//  RestartingScene.swift
//  GameAppInTvOS
//
//  Created by Igbinosa Idahosa on 12/2/18.
//  Copyright © 2018 Igbinosa Idahosa. All rights reserved.
//

import SpriteKit
//acts as a scence controller for the restart page that is viewed by the user
class RestartingScene: SKScene {
    //set feild variable
    var roundStr : String?
    //holds the current round number
    var round : Int?
    //holds SK label that is shown on screen
    var roundLabel : SKLabelNode!
    //holds clickable image button
    var newGameButton : SKSpriteNode!
    //holds value that determines if we save to remote SQL database
    var allowedToRestart : Bool?
    //holds touch display movement trail
    private var spinnyNode : SKShapeNode?
    
    //initializes feild variables
    override func didMove(to view: SKView) {
        
        // print("************\(allowedToRestart)*************")
        
        //initialize the feild variables to widgets
        roundLabel = self.childNode(withName: "//lblRoundTotal") as! SKLabelNode
        // roundLabel.text = "\(round)"
        
        //initialize the feild variables to widgets
        roundLabel.text = roundStr
        //initialize the feild variables to SK widgets
        newGameButton = self.childNode(withName: "//btnNewGame") as! SKSpriteNode
        newGameButton.texture = SKTexture(imageNamed: "newGamePicture")
        
        
        
        
        //Get label node from scene and store it for use later
        //Create shape node to use during mouse interaction
        let w = (self.size.width + self.size.height) * 0.05
        self.spinnyNode = SKShapeNode.init(rectOf: CGSize.init(width: w, height: w), cornerRadius: w * 0.3)
        
        //create touch movement animation that follows on screen touches
        if let spinnyNode = self.spinnyNode {
            spinnyNode.lineWidth = 2.5
            //run spin touch animation trail forever
            spinnyNode.run(SKAction.repeatForever(SKAction.rotate(byAngle: CGFloat(Double.pi), duration: 1)))
            //removes spin touch animation trail forever after some time
            spinnyNode.run(SKAction.sequence([SKAction.wait(forDuration: 0.5),
                                              SKAction.fadeOut(withDuration: 0.5),
                                              SKAction.removeFromParent()]))
        }
        
        
        
        
        
        
        // i would have to do the insertion in here and update the allowedToStart value when it ends
        
        //check if allowed to insert to remote SQL database
        if allowedToRestart == false{
            //stores url that we will be sending JSON object to
            let request = NSMutableURLRequest(url:NSURL(string: "http://idahosai.dev.fast.sheridanc.on.ca/workoutdata/jsonToSql.php")! as URL)
            //sets type of data transfer
            request.httpMethod = "POST"
            //sets the paremeters that we are transporting
            let postString = "a=\(String(describing: round!))"
            //does the necassary convertions so it is accepted in the transfer with no errors
            request.httpBody = postString.data(using: String.Encoding.utf8)
            //set specifics of the URLRequest so it gets accepted on by php file
            let task = URLSession.shared.dataTask(with: request as URLRequest){
                data, response, error in
                //checks if error occured while sending the parameters
                if (error != nil) {
                    // if there is an error allow them to still be able to go to the next page
                    //enable this page to be able to change now
                    self.allowedToRestart = true
                    // print("error\(error)")
                }
                //displays response message
                print("response = \(response)")
                //convert the response that you get to the datatypes that recognized and used often
                let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                //displays responseString message
                print("responseString = \(responseString)")
                // So it actually does take some time to get here, which is good
                //enable this page to be able to change now
                self.allowedToRestart = true
            }
            // it always gets to here also
            // self.allowedToRestart = true
            
            //run the block of code that inserts to SQL database
            task.resume()
            
            // self.allowedToRestart = true
            
            /*
             // when its optional ? put a ! at the end
             let params = "a="+"\(String(describing: round!))"
             print(params)
             let myUrl = "http://idahosai.dev.fast.sheridanc.on.ca/workoutdata/jsonToSql.php"
             let request = NSMutableURLRequest(url: NSURL(string: myUrl)! as URL)
             let session = URLSession.shared
             
             request.httpMethod = "POST"
             request.httpBody = params.data(using: String.Encoding.utf8)
             
             request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
             request.addValue("application/json", forHTTPHeaderField: "Accept")
             
             let task = session.dataTask(with: request as URLRequest) { data, response, error in
             guard data != nil else {
             print("no data found: \(error)")
             self.allowedToRestart = true
             return
             }
             print("data:\(data)")
             }
             allowedToRestart = true
             
             */
            
            
          

        }
        
    }
    
    
    
    
    //handles the begining of a touch to screen
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        //iterate through touches
        for t in touches { self.touchDown(atPoint: t.location(in: self)) }
        
        /*
        let touch = touches.first
        if let location = touch?.location(in:self){
            // returns all of the nodes close to that location
            let nodesArray = self.nodes(at: location)
            if nodesArray.first?.name == "btnNewGame" && allowedToRestart == true{
                let transition = SKTransition.flipHorizontal(withDuration: 0.2)
                // i must change this now that this is the first scenen shown
                //let gameScene = GameScene(size: self.size)
                let gameScene = BeginScene(fileNamed: "BeginScene")
                self.view?.presentScene(gameScene!, transition: transition)
            }
            
            
        }
        */
    }
    
    
    //handles the chage of scences
    func onToStartPage(){
        //with transition
        //computes page transiion
        let transition = SKTransition.doorsOpenHorizontal(withDuration: 0.2)
        //sets next scen to be the BeginScene (restart game)
        let beginScene = GamingScene(fileNamed: "BeginScene")
        self.view?.presentScene(beginScene!, transition: transition)
        
        // switch to next page imediately
        // let beginScene = BeginScene(fileNamed: "BeginScene")
        // self.view?.presentScene(beginScene!)
    }
    
    
    //moves character down
    func touchDown(atPoint pos : CGPoint) {
        //check existance of copy f SKShapeNode
        if let n = self.spinnyNode?.copy() as! SKShapeNode? {
            //set possioion
            n.position = pos
            //set color
            n.strokeColor = SKColor.green
            //add to scene
            self.addChild(n)
        }
    }
    //moves character
    func touchMoved(toPoint pos : CGPoint) {
        //check existance of copy f SKShapeNode
        if let n = self.spinnyNode?.copy() as! SKShapeNode? {
            n.position = pos
            //set color
            n.strokeColor = SKColor.blue
            //add to scene
            self.addChild(n)
        }
    }
    //move charcter up
    func touchUp(atPoint pos : CGPoint) {
        //check existance of copy f SKShapeNode
        if let n = self.spinnyNode?.copy() as! SKShapeNode? {
            //set possioion
            n.position = pos
            //set color
            n.strokeColor = SKColor.red
            //add to scene
            self.addChild(n)
        }
    }
    
    
    //handles when touch moves
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        //iterate though touches
        for t in touches { self.touchMoved(toPoint: t.location(in: self)) }
        
        //check if allowed to restart
        if(allowedToRestart == true){
            
            // run(SKAction.run(onToStartPage))
            
            //lets you wait a while then call function onToStartPage
            run(SKAction.sequence([SKAction.wait(forDuration: 2), SKAction.run(onToStartPage)]))
        }
        
        
    }
    //handles when touch ends
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        //iterate through touches
        for t in touches { self.touchUp(atPoint: t.location(in: self)) }
    }
    //handles when touch is canceled
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        //iterate though touches
        for t in touches { self.touchUp(atPoint: t.location(in: self)) }
    }
    //handles update
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
    }
}
