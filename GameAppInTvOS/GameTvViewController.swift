//
//  GameViewController.swift
//  GameAppInTvOS
//
//  Created by Igbinosa Idahosa on 12/2/18.
//  Copyright © 2018 Igbinosa Idahosa. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit


//it serves as the controller for a page view that the user interacts with in TvOS
class GameTvViewController: UIViewController {
//lets me perform additional initialization on views that were loaded
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let view = self.view as! SKView? {
            // Load the SKScene from 'GameScene.sks'
            if let scene = SKScene(fileNamed: "BeginScene") {
                // Set the scale mode to scale to fit the window
                scene.scaleMode = .aspectFill
                
                // Present the scene
                view.presentScene(scene)
            }
            
            view.ignoresSiblingOrder = true
            
            view.showsFPS = true
            view.showsNodeCount = true
        }
    }

    /*
    func returnToMainMenu(){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        guard  let storyboard = appDelegate.window?.rootViewController?.storyboard else { return }
        if let vc = storyboard.instantiateInitialViewController() {
            print("go to main menu")
            self.present(vc, animated: true, completion: nil)
        }
    }
    */
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }
    

}
