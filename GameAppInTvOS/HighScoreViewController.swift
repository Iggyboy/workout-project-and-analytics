//
//  HighScoreViewController.swift
//  GameAppInTvOS
//
//  Created by Igbinosa Idahosa on 12/2/18.
//  Copyright © 2018 Igbinosa Idahosa. All rights reserved.
//

import UIKit
//it serves as the controller for a page view that the user interacts with in TvOS
class HighScoreViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    let getData = GetData()
    var timer : Timer!
    var previousSavedScore : Int = 0
    var allHighScores : [Int] = []
    @IBOutlet var hsTableView : UITableView!
    
    //lets me perform additional initialization on views that were loaded
    override func viewDidLoad() {
        super.viewDidLoad()
        //calls one of my functions reapeatedly in a spaced out time interval until requirements are met
        self.timer = Timer.scheduledTimer(timeInterval: 0.4, target: self, selector: #selector(self.refreshHighScore), userInfo: nil, repeats: true)
        //produces an dictionary array of the high scores that the user has inputed into the database
        getData.jsonParserHighScores()

        // Do any additional setup after loading the view.
    }
    //this method is called when the system determines that the amount of available memory is low
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //checks if scores as been loaded into the array in another class (GetData), then fills feild array with the coresponding Routine objects
    @objc func refreshHighScore(){
        //check if there is a data in the score rellated array of getData
        if ((getData.dbScoresData.count) > 1){
            //stop the timer from continuing to call this function
            self.timer.invalidate()
            //print("going into forloop for previouse SavedScore!!!!!!!!")
            //print("dictionary array is : : \(getData.dbScoresData)")
            //loop through dictionary
            for i in getData.dbScoresData{
                allHighScores.append(Int("\(i["Score"]!)")!)
                /*
                print("gooooooo: \(i["Score"]!)")
                if previousSavedScore < Int("\(i["Score"]!)")!{
                    previousSavedScore = Int("\(i["Score"]!)")!
                    print("hiiiiiiiiii: \(previousSavedScore)")
                }
                */
                
            }
            //sort array in acending order
            allHighScores.sort()
            //revert array
            allHighScores.reverse()
            // print("out of loop now for previouse SavedScore!!!!!!!!!")
            
            //refresh table
            self.hsTableView.reloadData()
            
        }
    }
    //sets the number of tableView rows
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allHighScores.count
    }
    //sets height of tableView
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    //defines what a table cell looks like
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //get cell of table
        let tableCell = tableView.dequeueReusableCell(withIdentifier: "cell") ?? UITableViewCell()
        
        //get row current row number of table
        let rowNum = indexPath.row
        //set table cell's leble text value
        tableCell.textLabel?.text = "\(allHighScores[rowNum])"
        tableCell.accessoryType = .disclosureIndicator
        return tableCell
        
    }
    //defines whats happens when a tableView cell is selected
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //get row current row number of table
        let rowNum = indexPath.row
        //set alert button configurations
        let alertController = UIAlertController(title: "\(allHighScores[rowNum])", message: "", preferredStyle: .alert)
        //set cancel alert button configurations
        let cancelAction = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
        //place alert into alert controller to allow it to be allerted
        alertController.addAction(cancelAction)
        //do the alert now
        present(alertController, animated: true)
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
