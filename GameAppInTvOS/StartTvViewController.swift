//
//  StartTvViewController.swift
//  GameAppInTvOS
//
//  Created by Igbinosa Idahosa on 12/2/18.
//  Copyright © 2018 Igbinosa Idahosa. All rights reserved.
//

import UIKit
//it serves as the controller for a page view that the user interacts with in TvOS
class StartTvViewController: UIViewController {

    //lets me perform additional initialization on views that were loaded
    override func viewDidLoad() {
        super.viewDidLoad()

     
    }
    //this method is called when the system determines that the amount of available memory is low
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //handle navigation
    @IBAction func unwindToHomeViewController(segue : UIStoryboardSegue){
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
