//
//  BeginScene.swift
//  GameAppInTvOS
//
//  Created by Igbinosa Idahosa on 12/2/18.
//  Copyright © 2018 Igbinosa Idahosa. All rights reserved.
//

import SpriteKit
/*
protocol TransitionDelegate: SKSceneDelegate {
    func returnToMainMenu()
}
*/

//serves as a controller for the start of the game where the user clicks play
class BeginScene: SKScene {
    
    var newGameButton : SKSpriteNode!
    var tableButton : SKSpriteNode!
    private var spinnyNode : SKShapeNode?
    
    
    
    
    
    
    override func didMove(to view: SKView) {
        newGameButton = self.childNode(withName: "//newGameButton") as! SKSpriteNode
        
        // Get label node from scene and store it for use later
        
        
        // Create shape node to use during mouse interaction
        let w = (self.size.width + self.size.height) * 0.05
        self.spinnyNode = SKShapeNode.init(rectOf: CGSize.init(width: w, height: w), cornerRadius: w * 0.3)
        
        if let spinnyNode = self.spinnyNode {
            spinnyNode.lineWidth = 2.5
            
            spinnyNode.run(SKAction.repeatForever(SKAction.rotate(byAngle: CGFloat(Double.pi), duration: 1)))
            spinnyNode.run(SKAction.sequence([SKAction.wait(forDuration: 0.5),
                                              SKAction.fadeOut(withDuration: 0.5),
                                              SKAction.removeFromParent()]))
        }
        // i don't need this
        /*
        self.run(SKAction.wait(forDuration: 2),completion:{[unowned self] in
            guard let delegate = self.delegate else { return }
            self.view?.presentScene(nil)
            (delegate as! TransitionDelegate).returnToMainMenu()
        })
        */
        
    }
    // i don't need this
    /*
    deinit {
        print("\n THE SCENE \((type(of: self))) WAS REMOVED FROM MEMORY (DEINIT) \n")
    }
    */
    
    //overide touch
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        
        for t in touches { self.touchDown(atPoint: t.location(in: self)) }
        
        /*
        let touch = touches.first
        if let location = touch?.location(in:self){
            // returns all of the nodes close to that location
            let nodesArray = self.nodes(at: location)
            
            
            if nodesArray.first?.name == "newGameButton" {
                let transition = SKTransition.doorsOpenHorizontal(withDuration: 0.2)
                // i must change this now that this is the first scenen shown
                //let gameScene = GameScene(size: self.size)
                let gameScene = GamingScene(fileNamed: "GamingScene")
                self.view?.presentScene(gameScene!, transition: transition)
            }
            
            
        }
        */
    }
    
    
    func onToGame(){
        let transition = SKTransition.doorsOpenHorizontal(withDuration: 0.2)
        let gameScene = GamingScene(fileNamed: "GamingScene")
        self.view?.presentScene(gameScene!, transition: transition)
    }

    
    //These functions handle what happens when the user touches the screen
    func touchDown(atPoint pos : CGPoint) {
        if let n = self.spinnyNode?.copy() as! SKShapeNode? {
            n.position = pos
            n.strokeColor = SKColor.green
            self.addChild(n)
        }
    }
    
    func touchMoved(toPoint pos : CGPoint) {
        if let n = self.spinnyNode?.copy() as! SKShapeNode? {
            n.position = pos
            n.strokeColor = SKColor.blue
            self.addChild(n)
        }
    }
    
    func touchUp(atPoint pos : CGPoint) {
        if let n = self.spinnyNode?.copy() as! SKShapeNode? {
            n.position = pos
            n.strokeColor = SKColor.red
            self.addChild(n)
        }
    }
    

    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches { self.touchMoved(toPoint: t.location(in: self)) }
        
        run(SKAction.sequence([SKAction.wait(forDuration: 1), SKAction.run(onToGame)]))
      
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches { self.touchUp(atPoint: t.location(in: self)) }
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches { self.touchUp(atPoint: t.location(in: self)) }
    }
    
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
    }


 
}
