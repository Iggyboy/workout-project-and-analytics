//
//  WorkoutRoutinesViewController.swift
//  GameAppInTvOS
//
//  Created by Igbinosa Idahosa on 12/13/18.
//  Copyright © 2018 Igbinosa Idahosa. All rights reserved.
//

import UIKit
//it serves as the controller for a page view that the user interacts with in TvOS
class WorkoutRoutinesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    //set feild variables
    let getData = GetData()
    var timer : Timer!
    var timer2 : Timer!
    var timer3 : Timer!
    var previousSavedScore : Int = 0
    //set arrays
    var allHighScores : [Int] = []
    var allExercises : [Exercise] = []
    //var allSessions : [Session] = []
    var allRoutines : [Routine] = []
    var allSessions : [Session] = []
    //set UI elements
    @IBOutlet var workoutTableView : UITableView!
    @IBOutlet var lbSelectedNameValue : UILabel!
    @IBOutlet var lbSelectedExerciseValue : UILabel!
    @IBOutlet var lbSelectedPastDateValue : UILabel!
    
    //lets me perform additional initialization on views that were loaded
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //calls one of my functions reapeatedly in a spaced out time interval until requirements are met
        self.timer = Timer.scheduledTimer(timeInterval: 0.4, target: self, selector: #selector(self.refreshRoutines), userInfo: nil, repeats: true)
        //produces an dictionary array of the routine that the user has inputed into the database
        getData.jsonParserRoutines()
        //calls one of my functions reapeatedly in a spaced out time interval until requirements are met
        self.timer2 = Timer.scheduledTimer(timeInterval: 0.4, target: self, selector: #selector(self.refreshSession), userInfo: nil, repeats: true)
        //produces an dictionary array of the session that the user has inputed into the database
        getData.jsonParserSession()
        //calls one of my functions reapeatedly in a spaced out time interval until requirements are met
        self.timer3 = Timer.scheduledTimer(timeInterval: 0.4, target: self, selector: #selector(self.refreshExercise), userInfo: nil, repeats: true)
        //produces an dictionary array of the exercise that the user has inputed into the database
        getData.jsonParserExercise()
        
        
    }
    //this method is called when the system determines that the amount of available memory is low
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //checks if the routine data in the remote sql database has been received (GetData), then fills feild array with the those values
    @objc func refreshRoutines(){
        //check if there is a data in the routine rellated array of getData
        if ((getData.dbRoutinesData.count) > 1){
            //stop the timer from continuing to call this function
            self.timer.invalidate()
            // print("going into forloop for previouse SavedScore!!!!!!!!")
            // print("dictionary array is : : \(getData.dbRoutinesData)")
            
            //loop through dictionary
            for i in getData.dbRoutinesData{
                //store values of the dictionary to local variable
                let tid : Int = Int("\(i["routine_id"]!)")!
                let tname : String = "\(i["routine_name"]!)"
                //create new Routine object
                var Item = Routine()
                //fill up Routine object
                Item.routine_name = tname
                Item.routines_id = tid
                //store Routine object in an array of routine objects
                allRoutines.append(Item)
                
                //allRoutines.append(Int("\(i["Score"]!)")!)
                /*
                 print("gooooooo: \(i["Score"]!)")
                 if previousSavedScore < Int("\(i["Score"]!)")!{
                 previousSavedScore = Int("\(i["Score"]!)")!
                 print("hiiiiiiiiii: \(previousSavedScore)")
                 }
                 */
                
            }
            
            // allRoutines.sort()
            // allRoutines.reverse()
            // print("out of loop now for previouse SavedScore!!!!!!!!!")
            
            //refresh table
            self.workoutTableView.reloadData()
            
        }
    }
    //checks if the session data in the remote sql database has been received (GetData), then fills feild array with the those values
    @objc func refreshSession(){
        //check if there is a data in the session rellated array of getData
        if ((getData.dbSessionData.count) > 1){
            
            //stop the timer from continuing to call this function
            self.timer2.invalidate()
            // print("going into forloop for previouse SavedScore!!!!!!!!")
            // print("dictionary array is : : \(getData.dbSessionData)")
            
            //loop through dictionary
            for i in getData.dbSessionData{
                //store values of the dictionary to local variable
                let tsid : Int = Int("\(i["session_id"]!)")!
                let tid : Int = Int("\(i["routine_id"]!)")!
                let tdate : String = "\(i["session_date"]!)"
                //create new Seesion object
                var Item = Session()
                //fill up Session object
                Item.session_id = tsid
                Item.routines_id = tid
                Item.session_date = tdate
                //store Session object in an array of routine objects
                allSessions.append(Item)
                /*
                 print("gooooooo: \(i["Score"]!)")
                 if previousSavedScore < Int("\(i["Score"]!)")!{
                 previousSavedScore = Int("\(i["Score"]!)")!
                 print("hiiiiiiiiii: \(previousSavedScore)")
                 }
                 */
                
            }
            
            // allHighScores.sort()
            // allHighScores.reverse()
            // print("out of loop now for previouse SavedScore!!!!!!!!!")
            
            //refresh table
            self.workoutTableView.reloadData()
            
        }
    }
    //checks if the exercise data in the remote sql database has been received (GetData), then fills feild array with the those values
    @objc func refreshExercise(){
        //check if there is a data in the exercise rellated array of getData
        if ((getData.dbExerciseData.count) > 1){
            
            //stop the timer from continuing to call this function
            self.timer3.invalidate()
            // print("going into forloop for previouse SavedScore!!!!!!!!")
            // print("dictionary array is : : \(getData.dbExerciseData)")
            
            //loop through dictionary
            for i in getData.dbExerciseData{
                //store values of the dictionary to local variable
                let eid : Int = Int("\(i["exercise_id"]!)")!
                let ename : String = "\(i["exercise_name"]!)"
                let rid : Int = Int("\(i["routine_id"]!)")!
                let sid : Int = Int("\(i["session_id"]!)")!
                let setnum : Int = Int("\(i["set_number"]!)")!
                let reprange : String = "\(i["rep_range"]!)"
                let lastrep : Int = Int("\(i["last_rep"]!)")!
                let lweight : Int = Int("\(i["last_weight"]!)")!
                let rtimeafter : Int = Int("\(i["rest_time_after"]!)")!
                //create new Exercise object
                var Item = Exercise()
                //fill Exercise object
                Item.exercise_id = eid
                Item.exercise_name = ename
                Item.routine_id = rid
                Item.session_id = sid
                Item.set_number = setnum
                Item.rep_range = reprange
                Item.last_rep = lastrep
                Item.last_weight = lweight
                Item.rest_time_after = rtimeafter
                //store Exercise object in an array of routine objects
                allExercises.append(Item)
                
                // allHighScores.append(Int("\(i["Score"]!)")!)
                /*
                 print("gooooooo: \(i["Score"]!)")
                 if previousSavedScore < Int("\(i["Score"]!)")!{
                 previousSavedScore = Int("\(i["Score"]!)")!
                 print("hiiiiiiiiii: \(previousSavedScore)")
                 }
                 */
                
            }
            
            // allHighScores.sort()
            // allHighScores.reverse()
            // print("out of loop now for previouse SavedScore!!!!!!!!!")
            
            //refresh table
            self.workoutTableView.reloadData()
            
        }
    }
    //sets the number of tableView rows
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allRoutines.count
    }
    //sets height of tableView
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    //defines what a table cell looks like
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //get cell of table
        let tableCell = tableView.dequeueReusableCell(withIdentifier: "cell") ?? UITableViewCell()
        
        //get row current row number of table
        let rowNum = indexPath.row
        //set table cell's lable text value
        tableCell.textLabel?.text = "\(String(describing: allRoutines[rowNum].routine_name!)) with id: \(String(describing: allRoutines[rowNum].routines_id!))"
        tableCell.accessoryType = .disclosureIndicator
        
        
        //return tabllCell
        return tableCell
        
    }
    //defines whats happens when a tableView cell is selected
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //get row current row number of table
        let rowNum = indexPath.row
        //set name UI label text to the current tableCell's name
        lbSelectedNameValue.text = allRoutines[rowNum].routine_name
        
        //check if exercise dictionary has values
        if ((getData.dbExerciseData.count) > 1){
            //counter for the number of exercises in recreived dictionary that has
            //thesame routine id as the current table cell
            var counter : Int = 0
            //iterate through the array of exercizes
            for i in allExercises{
                //check what Routine's share thesame routine_id as the current tableCell selected
                if i.routine_id == allRoutines[rowNum].routines_id{
                    //increase counter
                    counter = counter + 1
                }
            }
            //set text of rellated UI label
            self.lbSelectedExerciseValue.text = "\(counter)"
            
        }
        //check if session dictionary has values
        if ((getData.dbSessionData.count) > 1){
            //iterate through the array of sessions
            for p in allSessions{
                //check what Session's share thesame routine_id as the current tableCell selected
                if p.routines_id == allRoutines[rowNum].routines_id{
                    //set text of rellated UI label
                    lbSelectedPastDateValue.text = "\(p.session_date!)"
                }
            }
        }
        
        // let alertController = UIAlertController(title: "\(String(describing: allRoutines[rowNum].routine_name))", message: "", preferredStyle: .alert)
        // let cancelAction = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
        // alertController.addAction(cancelAction)
        // present(alertController, animated: true)
        
    }

}
