//
//  GamingScene.swift
//  GameAppInTvOS
//
//  Created by Igbinosa Idahosa on 12/2/18.
//  Copyright © 2018 Igbinosa Idahosa. All rights reserved.
//

import SpriteKit
import GameplayKit


struct PhysicsCategory{
    static let None : UInt32 = 0
    static let All : UInt32 = UInt32.max
    static let Baddy : UInt32 = 0b1
    static let Hero : UInt32 = 0b10
    
    static let Projectile : UInt32 = 0b11
}

//import SKPhysicsContactDelegate to use all functionality of the physics world
class GamingScene: SKScene, SKPhysicsContactDelegate {
    //initialize feild variables
    //holds GetData object
    let getData = GetData()
    //holds a timmer
    var timer : Timer!
    //holds a timmer
    var timerTwo : Timer!
    //ininialize self explanatory feild variables
    //holds total number of workouts
    var amountOfWorkout = 0
    //holds index of current workout to be displayed
    var currentWorkoutPossition = 0
    //holds the previously saved score
    var previousSavedScore : Int = 0
    
    //initialize private feild SK variables
    private var label : SKLabelNode?
    private var spinnyNode : SKShapeNode?
    
    //sets background image
    var background = SKSpriteNode(imageNamed: "gym.png")
    //creates a SKSpriteNode for laster use
    private var sportNode : SKSpriteNode?
    //holds current lives of users
    private var lives : Int?
    //holds the lives incremental value
    let livesIncrement = 1
    //hold the SKLabel nodes that will be displayed on screen
    private var lblLives : SKLabelNode?
    private var lblRound : SKLabelNode?
    private var lblcurrentExercise : SKLabelNode?
    //holds current round that the user is on
    var currentRound = 0
    //golds SKEmitterNode for displaying when user gets hit
    var hurtFeild : SKEmitterNode!
    //holds array of SKTexture that will be iterated for the baad guy chracter picture animation
    var textureArray = [SKTexture]()
    //holds array of SKTexture that will be iterated for the player chracter picture animation
    var playerTextureArray = [SKTexture]()
    
    //this function always gets called when there is a movement in the game
    override func didMove(to view: SKView) {
        //calls one of my functions reapeatedly in a spaced out time interval until requirements are met
        self.timer = Timer.scheduledTimer(timeInterval: 0.4, target: self, selector: #selector(self.refreshLabel), userInfo: nil, repeats: true)
        //calls one of my functions reapeatedly in a spaced out time interval until requirements are met
        self.timerTwo = Timer.scheduledTimer(timeInterval: 0.4, target: self, selector: #selector(self.refreshHighScore), userInfo: nil, repeats: true)
        //produces an dictionary array of the routine that I have configured into the remote SQL database
        getData.jsonParser()
        //produces an dictionary array of the High scores that the user has inputed into the database
        getData.jsonParserHighScores()
        
        
        //go to GameScene and actually change the anchor to to x=0 and y=0 first
        background.position = CGPoint(x: frame.size.width/2, y: frame.size.height/2)
        
        // how to properly set the weight and height of a background image
        // could be this but the one i have now is better
        // background.size = CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        
        //set size of background image
        background.size = CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        //fade out background
        background.alpha = 0.2
        //add the stored background to the scene
        addChild(background)
        
        //initialize label node from scene and store it for it to be used later
        self.label = self.childNode(withName: "//helloLabel") as? SKLabelNode
        //checks if label exist
        if let label = self.label {
            //set alpha attribute hello lable
            label.alpha = 0.0
            //make hello label dispapear after some time
            label.run(SKAction.fadeIn(withDuration: 2.0))
        }
        
        //Create shape node to use during mouse interaction
        let w = (self.size.width + self.size.height) * 0.05
        //initialize SKShapeNode as a rectangular shape
        self.spinnyNode = SKShapeNode.init(rectOf: CGSize.init(width: w, height: w), cornerRadius: w * 0.3)
        //check if the newly create SKSapeNode exists
        if let spinnyNode = self.spinnyNode {
            //sets shape node line width
            spinnyNode.lineWidth = 2.5
            //make shape node rotate forever
            spinnyNode.run(SKAction.repeatForever(SKAction.rotate(byAngle: CGFloat(Double.pi), duration: 1)))
            //make shape node fade then remove from scene
            spinnyNode.run(SKAction.sequence([SKAction.wait(forDuration: 0.5),
                                              SKAction.fadeOut(withDuration: 0.5),
                                              SKAction.removeFromParent()]))
        }
        
        // spriteNode is realy used for dynamic creation in the code
        
        //Create the image character
        sportNode = SKSpriteNode(imageNamed: "workoutpose1.png")
        
        
        // sportNode?.position = CGPoint(x: 70, y: 90)
        
        //set position for image to appear, y moves the picture up, x moves the picture right
        sportNode?.position = CGPoint(x:frame.size.width * 0.5,y:frame.size.height * 0.05)
        //set width and length of image
        sportNode?.size = CGSize(width: UIScreen.main.bounds.width/8, height: UIScreen.main.bounds.height/12)
        //add image to the screen
        addChild(sportNode!)
        //set gravity attribute of our physical world
        physicsWorld.gravity = CGVector(dx:0,dy:0)
        //set our physical world to act on the current view that we are on
        physicsWorld.contactDelegate = self
        //sets the radius of the physical body
        sportNode?.physicsBody = SKPhysicsBody(circleOfRadius: (sportNode?.size.width)!/2)
        //makes main character physical body dynamic
        sportNode?.physicsBody?.isDynamic = true
        //makes main character physical body have categoryBitMask that points to the Hero
        sportNode?.physicsBody?.categoryBitMask = PhysicsCategory.Hero
        //set main characterthe physical body ability to be affected by bad guy
        sportNode?.physicsBody?.contactTestBitMask = PhysicsCategory.Baddy
        //sets main character physical body to have no collisionBitMask
        sportNode?.physicsBody?.collisionBitMask = PhysicsCategory.None
        //sets main character physical body to enable collition detection to be turned on
        sportNode?.physicsBody?.usesPreciseCollisionDetection = true
        
        
        //constantly add a bad guy to the screen that also calls the wait function for 1 second
        run(SKAction.repeatForever(SKAction.sequence([SKAction.run(addBaddy), SKAction.wait(forDuration: 1)])))
        //set lives that the user start with
        lives = 3
        //instantiat lives SKlabel on that is ready on the screen
        self.lblLives = self.childNode(withName: "//lives") as? SKLabelNode
        //set text of the livesSKlabel for user to see
        self.lblLives?.text = "Lives Left: \(lives!)"
        
        // what does this do many i should remove it cus it does nothing for my code, il get it later
        //check if SKlabel that follows user drag screen movements exist
        if let slabel = self.lblLives{
            //sets lebl alpha attribute
            slabel.alpha = 0.0
            //fade the SKLabel that follows user drag screen movements exist
            slabel.run(SKAction.fadeIn(withDuration: 2.0))
        }
        
        //initialize SKLabel that is slready on scene to the feild variable
        self.lblcurrentExercise = self.childNode(withName: "//currentExercise") as? SKLabelNode
        
        // *************************
        // get main delgates array of exercises ng loop through them
        // ************************
        // self.lblcurrentExercise?.text = "\(array[i]!)"
        
        //instantiat round SKlabel on that is ready on the screen
        self.lblRound = self.childNode(withName: "//round") as? SKLabelNode
        //make round SKlabel change wait 3 secondsbefor changing to the next round
        run(SKAction.repeatForever(SKAction.sequence([SKAction.wait(forDuration: 3), SKAction.run(increaseRound)])))
        //Create the enemy character that throws the balls
        let baddy = SKSpriteNode(imageNamed: "throwballpose11.png")
        //set enemy ball throwing chracter size
        baddy.size = CGSize(width: UIScreen.main.bounds.width/8, height: UIScreen.main.bounds.height/12)
        //filps the enemy ball throing character to face the left side
        baddy.xScale = baddy.xScale * -1
        //sets the initial position of the enemy ball throwing character
        baddy.position = CGPoint(x:frame.size.width * 0.93,y:frame.size.height * 0.05)
        //Create another enemy character that throws the balls
        let baddyTwo = SKSpriteNode(imageNamed: "throwballpose11.png")
        //set other enemy ball throwing chracter size
        baddyTwo.size = CGSize(width: UIScreen.main.bounds.width/8, height: UIScreen.main.bounds.height/12)
        // baddyTwo.xScale = baddy.xScale * -1
        //sets the initial position of second enemy ball throwing character
        baddyTwo.position = CGPoint(x:frame.size.width * 0.07,y:frame.size.height * 0.05)
        
        //add first enemy ball throwing character to the scene
        addChild(baddy)
        //add second enemy ball throwing character to the scene
        addChild(baddyTwo)
        //iterate through the main charcaters rellated pictures to create illution of the main character working out
        for i in 1 ..< 8{
            //appends ordered image into the array
            //sets the next main character image
            let textureName = "workoutpose\(i)"
            //adds the new image of the main character to the scene
            playerTextureArray.append(SKTexture(imageNamed: textureName))
            
        }
        //iterate throgh the enemy characters rellated pictures to create illution of the enemy character thowing a red ball
        for i in 1 ..< 12{
            //appends ordered image into the array
            //sets the next enemy character image
            let textureName = "throwballpose\(i)"
            //adds the new image of the enemy character to the scene
            textureArray.append(SKTexture(imageNamed: textureName))
            
        }
        //runs the picture change animation for the second enemy character  forever
        baddyTwo.run(SKAction.repeatForever(SKAction.sequence([SKAction.animate(with: textureArray, timePerFrame: 0.1)])))
        //runs the picture change animation for the first enemy character forever
        baddy.run(SKAction.repeatForever(SKAction.sequence([SKAction.animate(with: textureArray, timePerFrame: 0.1)])))
        //runs the picture change animation for the first enemy character forever
        sportNode?.run(SKAction.repeatForever(SKAction.sequence([SKAction.animate(with: playerTextureArray, timePerFrame: 0.1)])))
        
        // run(SKAction.repeatForever(SKAction.sequence([SKAction.run(imageAnimation)])))
    }
    //checks if the exercise data in the remote sql database has been received (GetData), then fills feild array with the those values
    @objc func refreshHighScore(){
        //checks if there are values in the dictionary
        if ((getData.dbScoresData.count) > 1){
            //stop the timer from continuing to call this function
            self.timerTwo.invalidate()
            // print("going into forloop for previouse SavedScore!!!!!!!!")
            // print("dictionary array is : : \(getData.dbScoresData)")
            
            //loop through dictionary
            for i in getData.dbScoresData{
                // print("gooooooo: \(i["Score"]!)")
                //check if score data in the dictionary is greater than the current score value
                if previousSavedScore < Int("\(i["Score"]!)")!{
                    // sets the current score value to current score itteration found in the dictionary
                    previousSavedScore = Int("\(i["Score"]!)")!
                    // print("hiiiiiiiiii: \(previousSavedScore)")
                }
                
                // print("\\\\\\\\\\\\\\\\\\\\\\\\")
                // print(i["Score"]!)
                // print("***********************", getData.dbScoresData)
                // print(Int("\(i["Score"]!)")!)
                // var oP : String = "\(String(describing: getData.dbScoresData![0]["Name"]!))"
                // print("**********",oP)
                // var eP : Int = Int(oP)!
                // print(eP)
                // print("/////////////////////////")
                // if oP != nil{
                // if Int("\(i["Score"]!)")! >= previousSavedScore{
                // previousSavedScore = Int("\(i["Score"]!)")!
                // print("**********",previousSavedScore)
                // }
                // }
                
            }
            // print("out of loop now for previouse SavedScore!!!!!!!!!")
            
        }
    }
    //checks if the data in the remote sql database has been received (GetData), then uses those values
    @objc func refreshLabel() {
        //checks if there are values in the dictionary
        if ((getData.dbData.count) > 0){
            //stop the timer from continuing to call this function
            self.timer.invalidate()
            //sets the count value to total count of dictionary
            amountOfWorkout = (getData.dbData.count)
            //makes current exercise change forever
            run(SKAction.repeatForever(SKAction.sequence([SKAction.wait(forDuration: 3), SKAction.run(changeWorkout)])))
            
        }
    }
    //decides what the current exercise will become next
    func changeWorkout(){
        
        // self.lblcurrentExercise = self.childNode(withName: "//currentExercise") as? SKLabelNode
        // adding the ! sign to the object being displays gets rid of the optional() that is printed along with the results
        
        //set what the current exercise is
        self.lblcurrentExercise?.text = "Current Exercise:\(String(describing: getData.dbData[currentWorkoutPossition]["Name"]!))"
        ////check if the current index for workout possition has reached it's last value
        if((currentWorkoutPossition + 1) == amountOfWorkout){
            //set current workout index back to zero
            currentWorkoutPossition = 0
            
        }
        else{
            //increases current workout index by 1
            currentWorkoutPossition = currentWorkoutPossition + 1
        }
    }
    
    
    
    
    /*
     func imageAnimation(){
     for i in 0 ..< 9{
     //append ordered image into the array
     let textureName = "throwballpose\(i)"
     textureArray.append(SKTexture(imageNamed: textureName))
     
     }
     }
     */
    
    //decides what the round will become next
    func increaseRound(){
        //increases the current round by 1
        currentRound = currentRound + 1
        
        // score = 0
        // **********blcoking this cus it's less efficient than putting it in the didview********
        // self.lblRound = self.childNode(withName: "//round") as? SKLabelNode
        
        //shows the current round to the user through using the SKLabel that is already on screen
        self.lblRound?.text = "Round: \(currentRound)"
    }
    //produces a random CFloat
    func random() ->CGFloat{
        //return a random CFloat
        return CGFloat(Float(arc4random()) / 0xffffffff)
    }
    
    //produces a random CGFloat based on 2 inputed parameter
    func random(min:CGFloat,max: CGFloat) -> CGFloat {
        //return a random CFloat
        return random() * (max-min) + min
    }
    //produces a random CGFloat based on 1 x position inputed parameter
    func randomX(x:CGFloat)->CGFloat{
        // this is the left and right (horizontal)
        
        //return a random CFloat
        return random(min:x-240,max:x+240)
    }
    //produces a random CFloat based on 1 y position inputed parameter
    func randomY(y:CGFloat)->CGFloat{
        // this is the up and down (verticle)
        //return a random CFloat
        return random(min:y-140,max:y+140)
    }
    
    //decides enemy balls that are added to the screen
    func addBaddy(){
        //create the enemy ball
        let baddy = SKSpriteNode(imageNamed: "ball.png")
        //create the another enemy ball
        let baddyTwo = SKSpriteNode(imageNamed: "ball.png")
        // could be this but the one i have now is better
        // baddy.size = CGSize(width: UIScreen.main.bounds.width/8, height: UIScreen.main.bounds.height/12)
        // if you modify the size you must do so here b4 its used so changes stay
        
       
        
        //set size of first enemy ball
        baddy.size = CGSize(width: UIScreen.main.bounds.width/16, height: UIScreen.main.bounds.height/24)
        //set scale of first enemy ball
        baddy.xScale = baddy.xScale * -1
        //set size of second enemy ball
        baddyTwo.size = CGSize(width: UIScreen.main.bounds.width/16, height: UIScreen.main.bounds.height/24)
        
        //set y position of first ball
        let actualY = randomY(y: (sportNode?.position.y)!)
        //set x position of first ball
        let actualX = randomX(x: (sportNode?.position.x)!)
        //set y position of second ball
        let actualYTwo = randomY(y: (sportNode?.position.y)!)
        //set x position of second ball
        let actualXTwo = randomX(x: (sportNode?.position.x)!)
        
        // the max is where the only half of the baddy's body is seen as he reaches the top of screen
        // let actualY = random(min:baddy.size.height/2, max:size.height-baddy.size.height/2)
        // make baddy appear just to the right of the screen
        // so size.width is thesame thing as frame.size.width
        // baddy.position = CGPoint(x:size.width + baddy.size.width/2,y:actualY)
        
        //set first enemy ball position
        baddy.position = CGPoint(x:frame.size.width * 0.86,y:frame.size.height * 0.11)
        //set second enemy ball position
        baddyTwo.position = CGPoint(x:frame.size.width * 0.14,y:frame.size.height * 0.11)
        //add first enemy ball position to scene
        addChild(baddy)
        //add second enemy ball position to scene
        addChild(baddyTwo)
        
        // were basically putting a sensor arround the bad guy, based on the eadges of the image
        baddyTwo.physicsBody = SKPhysicsBody(rectangleOf: baddy.size)
        baddyTwo.physicsBody?.isDynamic = true;
        //categorizing the bad balls
        baddyTwo.physicsBody?.categoryBitMask = PhysicsCategory.Baddy
        baddyTwo.physicsBody?.contactTestBitMask = PhysicsCategory.Hero
        baddyTwo.physicsBody?.collisionBitMask = PhysicsCategory.None
        
        //allows basically putting a sensor arround the bad guy, based on the eadges of the image
        baddy.physicsBody = SKPhysicsBody(rectangleOf: baddy.size)
        baddy.physicsBody?.isDynamic = true;
        //categorizing the bad guy
        baddy.physicsBody?.categoryBitMask = PhysicsCategory.Baddy
        baddy.physicsBody?.contactTestBitMask = PhysicsCategory.Hero
        baddy.physicsBody?.collisionBitMask = PhysicsCategory.None
        
        //set the ammount of time it will take to complete action 0 to 4 in this case
        let actualDuration = random(min:CGFloat(4.0),max:CGFloat(6.0))
        let actualDurationTwo = random(min:CGFloat(4.0),max:CGFloat(6.0))
        
        // create a move action for laster use. this will move a SKSpritNode to the left of the screen so they can be seen. The move position is still a point
        // block this for now
        // let actionMove = SKAction.move(to:CGPoint(x:-baddy.size.width/2, y: actualY),duration:TimeInterval(actualDuration))
        
        //create first enemy ball movement actions
        let actionMove = SKAction.move(to:CGPoint(x:actualX, y: actualY),duration:TimeInterval(actualDuration))
        let actionMoveDone = SKAction.removeFromParent()
        //create secondenemy ball movement actions
        let actionMoveTwo = SKAction.move(to:CGPoint(x:actualXTwo, y: actualYTwo),duration:TimeInterval(actualDurationTwo))
        //create aremove acion for later use
        let actionMoveDoneTwo = SKAction.removeFromParent()
        
        // run a action on  a SKSpritNode, action sequence so u decide what action runs next imediatte after a prior action
        //start the movement of the balls to the pre detemined possition
        baddy.run(SKAction.sequence([actionMove, actionMoveDone]))
        //start the movement of the balls to the pre detemined possition
        baddyTwo.run(SKAction.sequence([actionMoveTwo, actionMoveDoneTwo]))
    }
    
    func heroDidCollideWithBaddy(here:SKSpriteNode,baddy: SKSpriteNode){
        print("hit")
        
        hurtFeild = SKEmitterNode(fileNamed: "HitMagic")
        hurtFeild.position = (sportNode?.position)!
        self.addChild(hurtFeild)
        
        self.run(SKAction.playSoundFileNamed("", waitForCompletion: false))
        self.run(SKAction.wait(forDuration: 2)){
            //Reference to property 'hurtFeild' in closure requires explicit 'self.' to make capture semantics explicit
            self.hurtFeild.removeFromParent()
        }
        
        
        lives = lives! - livesIncrement
        self.lblLives?.text = "Lives Left: \(lives!)"
        if let slabel = self.lblLives {
            slabel.alpha = 0.0
            slabel.run(SKAction.fadeIn(withDuration: 2.0))
        }
        //when game ends
        if(lives! < 0){
            //****************************
            // Check remote SQL Database
            //if its greatest in the SQL database then add it to the SQL database then continue
            //***************************
            //let arr : [Int] = []
            //if((arr.count) < currentRound){
            print("previouseSavedScore = \(previousSavedScore)***********")
            if(previousSavedScore < currentRound){
                //add transition to Restart page
                let transition = SKTransition.flipHorizontal(withDuration: 0.3)
                //you new screen must be named on all its widget tabs or this won't work
                let restart = SKScene(fileNamed: "RestartingScene") as! RestartingScene
                restart.roundStr = "New High Round Reached!: \(self.currentRound)"
                restart.round = currentRound
                restart.allowedToRestart = false
                self.view?.presentScene(restart,transition: transition)
            }
            else{
                //add transition to Restart page
                let transition = SKTransition.flipHorizontal(withDuration: 0.3)
                //you new screen must be named on all its widget tabs or this won't work
                let restart = SKScene(fileNamed: "RestartingScene") as! RestartingScene
                restart.roundStr = "Round Reached: \(currentRound)"
                restart.allowedToRestart = true
                self.view?.presentScene(restart,transition: transition)
            }
            
        }
    }
    
    //Theses functions follow thesame definition for the beginScence page
    func didBegin(_ contact: SKPhysicsContact){
        var firstBody : SKPhysicsBody
        var secondBody : SKPhysicsBody
        
        if contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask{
            firstBody = contact .bodyA
            secondBody = contact.bodyB
        }
        else{
            firstBody = contact .bodyB
            secondBody = contact.bodyA
        }
        
        if((firstBody.categoryBitMask & PhysicsCategory.Baddy != 0) && (secondBody.categoryBitMask & PhysicsCategory.Hero != 0)){
            heroDidCollideWithBaddy(here: firstBody.node as! SKSpriteNode, baddy: secondBody.node as! SKSpriteNode)
        }
    }
    
    //move good guy
    func moveGoodGuy(toPoint pos: CGPoint){
        let actionMove = SKAction.move(to:pos, duration: TimeInterval(2.0))
        
        //let actionMoveDone = SKAction.rotate(byAngle: CGFloat(360.0), duration: TimeInterval(2.0))
        
        //sportNode?.run(SKAction.sequence([actionMove, actionMoveDone]))
        sportNode?.run(SKAction.sequence([actionMove]))
    }
    //These funcions follow thesame definition for the beigin screens page
    func touchDown(atPoint pos : CGPoint) {
        if let n = self.spinnyNode?.copy() as! SKShapeNode? {
            n.position = pos
            n.strokeColor = SKColor.green
            self.addChild(n)
        }
        moveGoodGuy(toPoint: pos)
    }
    
    func touchMoved(toPoint pos : CGPoint) {
        if let n = self.spinnyNode?.copy() as! SKShapeNode? {
            n.position = pos
            n.strokeColor = SKColor.blue
            self.addChild(n)
        }
        moveGoodGuy(toPoint: pos)
    }
    
    func touchUp(atPoint pos : CGPoint) {
        if let n = self.spinnyNode?.copy() as! SKShapeNode? {
            n.position = pos
            n.strokeColor = SKColor.red
            self.addChild(n)
        }
    }
    //these functions follow thesame  definition for the beginScene page
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let label = self.label {
            label.run(SKAction.init(named: "Pulse")!, withKey: "fadeInOut")
        }
        
        for t in touches { self.touchDown(atPoint: t.location(in: self)) }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches { self.touchMoved(toPoint: t.location(in: self)) }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches { self.touchUp(atPoint: t.location(in: self)) }
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches { self.touchUp(atPoint: t.location(in: self)) }
    }
    
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
    }
}
