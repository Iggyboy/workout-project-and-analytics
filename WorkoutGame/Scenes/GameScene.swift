//
//  GameScene.swift
//  WorkoutGame
//
//  Created by Igbinosa Idahosa on 2018-11-12.
//  Copyright © 2018 Igbinosa Idahosa. All rights reserved.
//

//THE FORMAT FOR THIS file FOLLOWS THE EXACT SAME CODE AS THE BeginScene,swift file so refer there to see the indept comments FOR WHAT I EXACTLY THESAME CODE


import SpriteKit
import GameplayKit

//responsible of collitions in game
struct PhysicsCategory{
    static let None : UInt32 = 0
    static let All : UInt32 = UInt32.max
    static let Baddy : UInt32 = 0b1
    static let Hero : UInt32 = 0b10
    
    static let Projectile : UInt32 = 0b11
}

//import SKPhysicsContactDelegate to use all functionality of the physics world
class GameScene: SKScene, SKPhysicsContactDelegate {
    
    let getData = GetData()
    var timer : Timer!
    var timerTwo : Timer!
    var amountOfWorkout = 0
    var currentWorkoutPossition = 0
    var previousSavedScore : Int = 0
    
    
    private var label : SKLabelNode?
    private var spinnyNode : SKShapeNode?
    
    //bring in an image
    var background = SKSpriteNode(imageNamed: "gym.png")
    private var sportNode : SKSpriteNode?
    
    private var lives : Int?
    let livesIncrement = 1
    private var lblLives : SKLabelNode?
    private var lblRound : SKLabelNode?
    private var lblcurrentExercise : SKLabelNode?
    var currentRound = 0
    var hurtFeild : SKEmitterNode!
    var textureArray = [SKTexture]()
    var playerTextureArray = [SKTexture]()
    
    //this function always gets called when there is a movement in the game
    override func didMove(to view: SKView) {
        
        self.timer = Timer.scheduledTimer(timeInterval: 0.4, target: self, selector: #selector(self.refreshLabel), userInfo: nil, repeats: true)
        self.timerTwo = Timer.scheduledTimer(timeInterval: 0.4, target: self, selector: #selector(self.refreshHighScore), userInfo: nil, repeats: true)
        //getData.jsonParser()
        getData.jsonParserExercise()
        getData.jsonParserHighScores()

        
        //go to GameScene and actually change the anchor to to x=0 and y=0 first
        background.position = CGPoint(x: frame.size.width/2, y: frame.size.height/2)
        //how to properly set the weight and height of a background image
        //could be this but the one i have now is better
        //background.size = CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        background.size = CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        //fade out background
        background.alpha = 0.2
        addChild(background)
        
        // Get label node from scene and store it for use later
        self.label = self.childNode(withName: "//helloLabel") as? SKLabelNode
        if let label = self.label {
            label.alpha = 0.0
            label.run(SKAction.fadeIn(withDuration: 2.0))
        }
        
        // Create shape node to use during mouse interaction
        let w = (self.size.width + self.size.height) * 0.05
        self.spinnyNode = SKShapeNode.init(rectOf: CGSize.init(width: w, height: w), cornerRadius: w * 0.3)
        
        if let spinnyNode = self.spinnyNode {
            spinnyNode.lineWidth = 2.5
            
            spinnyNode.run(SKAction.repeatForever(SKAction.rotate(byAngle: CGFloat(Double.pi), duration: 1)))
            spinnyNode.run(SKAction.sequence([SKAction.wait(forDuration: 0.5),
                                              SKAction.fadeOut(withDuration: 0.5),
                                              SKAction.removeFromParent()]))
        }
        
        // Create the image character//spriteNode is realy used for dynamic creation in the code
        sportNode = SKSpriteNode(imageNamed: "workoutpose1.png")
        //set position for image to appear, y moves the picture up, x moves the picture right
        
        //sportNode?.position = CGPoint(x: 70, y: 90)
        sportNode?.position = CGPoint(x:frame.size.width * 0.5,y:frame.size.height * 0.05)
        //set width and length of image
        sportNode?.size = CGSize(width: UIScreen.main.bounds.width/8, height: UIScreen.main.bounds.height/12)
        //add image to the screen
        addChild(sportNode!)
        
        physicsWorld.gravity = CGVector(dx:0,dy:0)
        physicsWorld.contactDelegate = self
        
        sportNode?.physicsBody = SKPhysicsBody(circleOfRadius: (sportNode?.size.width)!/2)
        sportNode?.physicsBody?.isDynamic = true
        sportNode?.physicsBody?.categoryBitMask = PhysicsCategory.Hero
        sportNode?.physicsBody?.contactTestBitMask = PhysicsCategory.Baddy
        sportNode?.physicsBody?.collisionBitMask = PhysicsCategory.None
        sportNode?.physicsBody?.usesPreciseCollisionDetection = true
        
        
        //run a reapeaForever function that also calls the wait function
        run(SKAction.repeatForever(SKAction.sequence([SKAction.run(addBaddy), SKAction.wait(forDuration: 1)])))
        // step 11b - init score
        lives = 3
        self.lblLives = self.childNode(withName: "//lives") as? SKLabelNode
        self.lblLives?.text = "Lives Left: \(lives!)"
        
        // what does this do many i should remove it cus it does nothing for my code, il get it later
        if let slabel = self.lblLives{
            slabel.alpha = 0.0
            slabel.run(SKAction.fadeIn(withDuration: 2.0))
        }
        
        
        self.lblcurrentExercise = self.childNode(withName: "//currentExercise") as? SKLabelNode
        //*************************
        // get main delgates array of exercises ng loop through them
        //************************
        //self.lblcurrentExercise?.text = "\(array[i]!)"
        
        self.lblRound = self.childNode(withName: "//round") as? SKLabelNode
        
        run(SKAction.repeatForever(SKAction.sequence([SKAction.wait(forDuration: 3), SKAction.run(increaseRound)])))
        
        let baddy = SKSpriteNode(imageNamed: "throwballpose11.png")
        baddy.size = CGSize(width: UIScreen.main.bounds.width/8, height: UIScreen.main.bounds.height/12)
        baddy.xScale = baddy.xScale * -1
        baddy.position = CGPoint(x:frame.size.width * 0.93,y:frame.size.height * 0.05)
        
        let baddyTwo = SKSpriteNode(imageNamed: "throwballpose11.png")
        baddyTwo.size = CGSize(width: UIScreen.main.bounds.width/8, height: UIScreen.main.bounds.height/12)
        //baddyTwo.xScale = baddy.xScale * -1
        baddyTwo.position = CGPoint(x:frame.size.width * 0.07,y:frame.size.height * 0.05)
        
        //your surposed to add them to the scene after you initialize its variables
        addChild(baddy)
        
        addChild(baddyTwo)
        
        for i in 1 ..< 8{
            //append ordered image into the array
            let textureName = "workoutpose\(i)"
            playerTextureArray.append(SKTexture(imageNamed: textureName))
            
        }
        
        for i in 1 ..< 12{
            //append ordered image into the array
            let textureName = "throwballpose\(i)"
            textureArray.append(SKTexture(imageNamed: textureName))
            
        }
        baddyTwo.run(SKAction.repeatForever(SKAction.sequence([SKAction.animate(with: textureArray, timePerFrame: 0.1)])))
        baddy.run(SKAction.repeatForever(SKAction.sequence([SKAction.animate(with: textureArray, timePerFrame: 0.1)])))
        sportNode?.run(SKAction.repeatForever(SKAction.sequence([SKAction.animate(with: playerTextureArray, timePerFrame: 0.1)])))
        //run(SKAction.repeatForever(SKAction.sequence([SKAction.run(imageAnimation)])))
    }
    @objc func refreshHighScore(){
        if ((getData.dbScoresData.count) > 1){
            //stop the timer from continuing to call this function
            self.timerTwo.invalidate()
            print("going into forloop for previouse SavedScore!!!!!!!!")
            print("dictionary array is : : \(getData.dbScoresData)")
            for i in getData.dbScoresData{
                print("gooooooo: \(i["Score"]!)")
                if previousSavedScore < Int("\(i["Score"]!)")!{
                    previousSavedScore = Int("\(i["Score"]!)")!
                    print("hiiiiiiiiii: \(previousSavedScore)")
                }
                //print("\\\\\\\\\\\\\\\\\\\\\\\\")
                //print(i["Score"]!)
                //print("***********************", getData.dbScoresData)
                //print(Int("\(i["Score"]!)")!)
                //var oP : String = "\(String(describing: getData.dbScoresData![0]["Name"]!))"
                //print("**********",oP)
                //var eP : Int = Int(oP)!
                //print(eP)
                //print("/////////////////////////")
                //if oP != nil{
                    //if Int("\(i["Score"]!)")! >= previousSavedScore{
                        //previousSavedScore = Int("\(i["Score"]!)")!
                        //print("**********",previousSavedScore)
                    //}
                //}

            }
            print("out of loop now for previouse SavedScore!!!!!!!!!")
            
        }
    }
    
    @objc func refreshLabel() {
        if ((getData.dbExerciseData.count) > 0){
            //stop the timer from continuing to call this function
            self.timer.invalidate()
            amountOfWorkout = (getData.dbExerciseData.count)
            run(SKAction.repeatForever(SKAction.sequence([SKAction.wait(forDuration: 3), SKAction.run(changeWorkout)])))
            
        }
    }
    
    func changeWorkout(){
       
        //self.lblcurrentExercise = self.childNode(withName: "//currentExercise") as? SKLabelNode
        //adding the ! sign to the object being displays gets rid of the optional() that is printed along with the results
        self.lblcurrentExercise?.text = "Current Exercise:\(String(describing: getData.dbExerciseData[currentWorkoutPossition]["exercise_name"]!))"
        //let ename : String = "\(i["exercise_name"]!)"
        
        if((currentWorkoutPossition + 1) == amountOfWorkout){
            currentWorkoutPossition = 0
            
        }
        else{
            currentWorkoutPossition = currentWorkoutPossition + 1
        }
    }
    
    
    
    
    /*
    func imageAnimation(){
        for i in 0 ..< 9{
            //append ordered image into the array
            let textureName = "throwballpose\(i)"
            textureArray.append(SKTexture(imageNamed: textureName))
            
        }
    }
    */
    func increaseRound(){
        currentRound = currentRound + 1
        //score = 0
        //**********blcoking this cus it's less efficient than putting it in the didview********
        //self.lblRound = self.childNode(withName: "//round") as? SKLabelNode
        self.lblRound?.text = "Round: \(currentRound)"
    }
    
    func random() ->CGFloat{
        return CGFloat(Float(arc4random()) / 0xffffffff)
    }
    
    
    func random(min:CGFloat,max: CGFloat) -> CGFloat {
        return random() * (max-min) + min
    }
    func randomX(x:CGFloat)->CGFloat{
        //this is the left and right (horizontal)
        return random(min:x-240,max:x+240)
    }
    
    func randomY(y:CGFloat)->CGFloat{
        //this is the up and down (verticle)
        return random(min:y-140,max:y+140)
    }
    
    func addBaddy(){
        let baddy = SKSpriteNode(imageNamed: "ball.png")
        let baddyTwo = SKSpriteNode(imageNamed: "ball.png")
        //could be this but the one i have now is better
        //baddy.size = CGSize(width: UIScreen.main.bounds.width/8, height: UIScreen.main.bounds.height/12)
        //if you modify the size you must do so here b4 its used so changes stay
        
        //8 and 12
        baddy.size = CGSize(width: UIScreen.main.bounds.width/16, height: UIScreen.main.bounds.height/24)
        baddy.xScale = baddy.xScale * -1
        
        baddyTwo.size = CGSize(width: UIScreen.main.bounds.width/16, height: UIScreen.main.bounds.height/24)
        
        
        let actualY = randomY(y: (sportNode?.position.y)!)
        let actualX = randomX(x: (sportNode?.position.x)!)
        
        let actualYTwo = randomY(y: (sportNode?.position.y)!)
        let actualXTwo = randomX(x: (sportNode?.position.x)!)
        
        // the max is where the only half of the baddy's body is seen as he reaches the top of screen
        //let actualY = random(min:baddy.size.height/2, max:size.height-baddy.size.height/2)
        //make baddy appear just to the right of the screen
        //so size.width is thesame thing as frame.size.width
        //baddy.position = CGPoint(x:size.width + baddy.size.width/2,y:actualY)
        baddy.position = CGPoint(x:frame.size.width * 0.86,y:frame.size.height * 0.11)
        
        baddyTwo.position = CGPoint(x:frame.size.width * 0.14,y:frame.size.height * 0.11)

        addChild(baddy)
        
        addChild(baddyTwo)
        
        //were basically putting a sensor arround the bad guy, based on the eadges of the image
        baddyTwo.physicsBody = SKPhysicsBody(rectangleOf: baddy.size)
        baddyTwo.physicsBody?.isDynamic = true;
        //categorizing the bad guy
        baddyTwo.physicsBody?.categoryBitMask = PhysicsCategory.Baddy
        baddyTwo.physicsBody?.contactTestBitMask = PhysicsCategory.Hero
        baddyTwo.physicsBody?.collisionBitMask = PhysicsCategory.None
        
        //were basically putting a sensor arround the bad guy, based on the eadges of the image
        baddy.physicsBody = SKPhysicsBody(rectangleOf: baddy.size)
        baddy.physicsBody?.isDynamic = true;
        //categorizing the bad guy
        baddy.physicsBody?.categoryBitMask = PhysicsCategory.Baddy
        baddy.physicsBody?.contactTestBitMask = PhysicsCategory.Hero
        baddy.physicsBody?.collisionBitMask = PhysicsCategory.None
        
        //set the ammount of time it will take to complete action 0 to 4 in this case
        let actualDuration = random(min:CGFloat(4.0),max:CGFloat(6.0))
        
        let actualDurationTwo = random(min:CGFloat(4.0),max:CGFloat(6.0))
        
        // create a move action for laster use. this will move a SKSpritNode to the left of the screen so they can be seen. The move position is still a point
        //block this for now
        //let actionMove = SKAction.move(to:CGPoint(x:-baddy.size.width/2, y: actualY),duration:TimeInterval(actualDuration))
        let actionMove = SKAction.move(to:CGPoint(x:actualX, y: actualY),duration:TimeInterval(actualDuration))
        //create aremove acion for later use
        let actionMoveDone = SKAction.removeFromParent()
        
        let actionMoveTwo = SKAction.move(to:CGPoint(x:actualXTwo, y: actualYTwo),duration:TimeInterval(actualDurationTwo))
        //create aremove acion for later use
        let actionMoveDoneTwo = SKAction.removeFromParent()
        
        //run a action on  a SKSpritNode, action sequence so u decide what action runs next imediatte after a prior action
        baddy.run(SKAction.sequence([actionMove, actionMoveDone]))
        
        baddyTwo.run(SKAction.sequence([actionMoveTwo, actionMoveDoneTwo]))
    }
    
    func heroDidCollideWithBaddy(here:SKSpriteNode,baddy: SKSpriteNode){
        print("hit")
        
        hurtFeild = SKEmitterNode(fileNamed: "HitMagic")
        hurtFeild.position = (sportNode?.position)!
        self.addChild(hurtFeild)
        
        self.run(SKAction.playSoundFileNamed("", waitForCompletion: false))
        self.run(SKAction.wait(forDuration: 2)){
            //Reference to property 'hurtFeild' in closure requires explicit 'self.' to make capture semantics explicit
            self.hurtFeild.removeFromParent()
        }
        
        
        lives = lives! - livesIncrement
        self.lblLives?.text = "Lives Left: \(lives!)"
        if let slabel = self.lblLives {
            slabel.alpha = 0.0
            slabel.run(SKAction.fadeIn(withDuration: 2.0))
        }
        //when game ends
        if(lives! < 0){
            //****************************
            // Check remote SQL Database
            //if its greatest in the SQL database then add it to the SQL database then continue
            //***************************
            //let arr : [Int] = []
            //if((arr.count) < currentRound){
            print("previouseSavedScore = \(previousSavedScore)***********")
            if(previousSavedScore < currentRound){
                //add transition to Restart page
                let transition = SKTransition.flipHorizontal(withDuration: 0.3)
                //you new screen must be named on all its widget tabs or this won't work
                let restart = SKScene(fileNamed: "RestartScene") as! RestartScene
                restart.roundStr = "New High Round Reached!: \(self.currentRound)"
                restart.round = currentRound
                restart.allowedToRestart = false
                self.view?.presentScene(restart,transition: transition)
            }
            else{
                //add transition to Restart page
                let transition = SKTransition.flipHorizontal(withDuration: 0.3)
                //you new screen must be named on all its widget tabs or this won't work
                let restart = SKScene(fileNamed: "RestartScene") as! RestartScene
                restart.roundStr = "Round Reached: \(currentRound)"
                self.view?.presentScene(restart,transition: transition)
            }

        }
    }
    
    
    func didBegin(_ contact: SKPhysicsContact){
        var firstBody : SKPhysicsBody
        var secondBody : SKPhysicsBody
        
        if contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask{
            firstBody = contact .bodyA
            secondBody = contact.bodyB
        }
        else{
            firstBody = contact .bodyB
            secondBody = contact.bodyA
        }
        
        if((firstBody.categoryBitMask & PhysicsCategory.Baddy != 0) && (secondBody.categoryBitMask & PhysicsCategory.Hero != 0)){
            heroDidCollideWithBaddy(here: firstBody.node as! SKSpriteNode, baddy: secondBody.node as! SKSpriteNode)
        }
    }
    
    //move good guy
    func moveGoodGuy(toPoint pos: CGPoint){
        let actionMove = SKAction.move(to:pos, duration: TimeInterval(2.0))
        
        //let actionMoveDone = SKAction.rotate(byAngle: CGFloat(360.0), duration: TimeInterval(2.0))
        
        //sportNode?.run(SKAction.sequence([actionMove, actionMoveDone]))
        sportNode?.run(SKAction.sequence([actionMove]))
    }
    
    func touchDown(atPoint pos : CGPoint) {
        if let n = self.spinnyNode?.copy() as! SKShapeNode? {
            n.position = pos
            n.strokeColor = SKColor.green
            self.addChild(n)
        }
        moveGoodGuy(toPoint: pos)
    }
    
    func touchMoved(toPoint pos : CGPoint) {
        if let n = self.spinnyNode?.copy() as! SKShapeNode? {
            n.position = pos
            n.strokeColor = SKColor.blue
            self.addChild(n)
        }
        moveGoodGuy(toPoint: pos)
    }
    
    func touchUp(atPoint pos : CGPoint) {
        if let n = self.spinnyNode?.copy() as! SKShapeNode? {
            n.position = pos
            n.strokeColor = SKColor.red
            self.addChild(n)
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let label = self.label {
            label.run(SKAction.init(named: "Pulse")!, withKey: "fadeInOut")
        }
        
        for t in touches { self.touchDown(atPoint: t.location(in: self)) }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches { self.touchMoved(toPoint: t.location(in: self)) }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches { self.touchUp(atPoint: t.location(in: self)) }
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches { self.touchUp(atPoint: t.location(in: self)) }
    }
    
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
    }
}
