//
//  MenuScene.swift
//  WorkoutGame
//
//  Created by Igbinosa Idahosa on 11/17/18.
//  Copyright © 2018 Igbinosa Idahosa. All rights reserved.
//

//change to spritekit
import SpriteKit
//acts as a controller for the menu that the user sees
class MenuScene: SKScene {

    var newGameButton : SKSpriteNode!
    var tableButton : SKSpriteNode!
    
    
    
    
    
    
    override func didMove(to view: SKView) {
        newGameButton = self.childNode(withName: "//newGameButton") as! SKSpriteNode
    
        
    }
    
    
    
    
    //overide touch
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        
        if let location = touch?.location(in:self){
            // returns all of the nodes close to that location
            let nodesArray = self.nodes(at: location)
            
            
            if nodesArray.first?.name == "newGameButton" {
                let transition = SKTransition.doorsOpenHorizontal(withDuration: 0.2)
                // i must change this now that this is the first scenen shown
                //let gameScene = GameScene(size: self.size)
                let gameScene = GameScene(fileNamed: "GameScene")
                self.view?.presentScene(gameScene!, transition: transition)
            }

            
        }
    }
}
