//
//  RestartScene.swift
//  WorkoutGame
//
//  Created by Igbinosa Idahosa on 11/18/18.
//  Copyright © 2018 Igbinosa Idahosa. All rights reserved.
//

import SpriteKit
//acts as a scence controller for the restart page that is viewed by the user
class RestartScene: SKScene {
    //set feild variable
    var roundStr : String?
    //holds the current round number
    var round : Int?
    //holds SK label that is shown on screen
    var roundLabel : SKLabelNode!
    //holds clickable image button
    var newGameButton : SKSpriteNode!
    //holds value that determines if we save to remote SQL database
    var allowedToRestart : Bool = true
    
    //initializes feild variables
    override func didMove(to view: SKView) {
        //initialize the feild variables to widgets
        // print("************\(allowedToRestart)*************")
        roundLabel = self.childNode(withName: "//lblRoundTotal") as! SKLabelNode
        // roundLabel.text = "\(round)"
        roundLabel.text = roundStr
        
        //initialize the feild variables to Sk widgets
        newGameButton = self.childNode(withName: "//btnNewGame") as! SKSpriteNode
        newGameButton.texture = SKTexture(imageNamed: "newGamePicture")
        
        
        // i would have to do the insertion in here and update the allowedToStart value when it ends
        
        //check if allowed to insert to remote SQL database
        if allowedToRestart == false{
            //stores url that we will be sending JSON object to
            let request = NSMutableURLRequest(url:NSURL(string: "http://idahosai.dev.fast.sheridanc.on.ca/workoutdata/jsonToSql.php")! as URL)
            //sets type of data transfer
            request.httpMethod = "POST"
            //sets the paremeters that we are transporting
            let postString = "a=\(String(describing: round!))"
            //does the necassary convertions so it is accepted in the transfer with no errors
            request.httpBody = postString.data(using: String.Encoding.utf8)
            //set specifics of the URLRequest so it gets accepted on by php file
            let task = URLSession.shared.dataTask(with: request as URLRequest){
                data, response, error in
                //checks if error occured while sending the parameters
                if (error != nil) {
                    //displays error message
                    print("error\(error)")
                }
                //displays response message
                print("response = \(response)")
                //convert the response that you get to the datatypes that recognized and used often
                let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                //displays responseString message
                print("responseString = \(responseString)")
                //enable this page to be able to change now
                self.allowedToRestart = true
            }
            //enable this page to be able to change now
            self.allowedToRestart = true
            //run the block of code that inserts to SQL database
            task.resume()
            
            /*
            // when its optional ? put a ! at the end
            let params = "a="+"\(String(describing: round!))"
            print(params)
            let myUrl = "http://idahosai.dev.fast.sheridanc.on.ca/workoutdata/jsonToSql.php"
            let request = NSMutableURLRequest(url: NSURL(string: myUrl)! as URL)
            let session = URLSession.shared
            
            request.httpMethod = "POST"
            request.httpBody = params.data(using: String.Encoding.utf8)
            
            request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            
            let task = session.dataTask(with: request as URLRequest) { data, response, error in
                guard data != nil else {
                    print("no data found: \(error)")
                    self.allowedToRestart = true
                    return
                }
                print("data:\(data)")
            }
            allowedToRestart = true
            
            */
        }
        
    }
    
    
    
    
    //handles the begining of a touch to screen
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        //store the first touch to screen
        let touch = touches.first
        //check the possition of the stored first touch
        if let location = touch?.location(in:self){
            //returns all of the nodes close to that location
            let nodesArray = self.nodes(at: location)
            
            //get first node that is return in the array (get closest node that the user selected)
            if nodesArray.first?.name == "btnNewGame" && allowedToRestart == true{
                //configure transition details
                let transition = SKTransition.flipHorizontal(withDuration: 0.2)
                // i must change this now that this is the first scenen shown
                // let gameScene = GameScene(size: self.size)
                
                //stores the next scene that the current page will soon go to
                let gameScene = GameScene(fileNamed: "MenuScene")
                //goes to the sored scene and transitions now
                self.view?.presentScene(gameScene!, transition: transition)
            }
            
            
        }
    }
}
