//
//  Exercise.swift
//  WorkoutGame
//
//  Created by Igbinosa Idahosa on 11/18/18.
//  Copyright © 2018 Igbinosa Idahosa. All rights reserved.
//

import UIKit
//represents the Exercise in the remote sql database
class Exercise: NSObject {

    var exercise_id : Int?
    var exercise_name : String?
    var routine_id : Int?
    var session_id : Int?
    var set_number : Int?
    var rep_range : String?
    var last_rep : Int?
    var last_weight : Int?
    var rest_time_after : Int?
    
    
}
