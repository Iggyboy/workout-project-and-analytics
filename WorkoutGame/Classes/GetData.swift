//
//  GetData.swift
//  WorkoutGame
//
//  Created by Igbinosa Idahosa on 12/1/18.
//  Copyright © 2018 Igbinosa Idahosa. All rights reserved.
//

import UIKit
//does the async functioncalls to retrieve an put data in to the remote sql database
class GetData: NSObject {

    var dbData : [NSDictionary] = []
    var dbScoresData : [NSDictionary] = []
    
    var dbExerciseData : [NSDictionary] = []
    var dbRoutinesData : [NSDictionary] = []
    var dbSessionData : [NSDictionary] = []
    
    let myUrl = "http://idahosai.dev.fast.sheridanc.on.ca/workoutdata/sqlRoutinesToJson.php" as String
    let myScoresUrl = "http://idahosai.dev.fast.sheridanc.on.ca/workoutdata/jsonToSqlScores.php"
    
    let myExerciseUrl = "http://aguilaar.dev.fast.sheridanc.on.ca/prog31975/selectExercise.php"
    let myRoutinesUrl = "http://aguilaar.dev.fast.sheridanc.on.ca/prog31975/selectRoutine.php"
    let mySessionUrl = "http://aguilaar.dev.fast.sheridanc.on.ca/prog31975/selectSession.php"
    
    
    enum JSONError : String, Error {
        case NoData = "Error: No Data"
        case ConversionFailed = "Error: conversion from JSON Failed"
        
    }
    //get data from remote sql data
    func jsonParser(){
        //url object
        guard let endpoint = URL(string: myUrl) else{
            print("Error creating connection")
            return
        }
        //url request object
        let request = URLRequest(url: endpoint)
        //get data by asynce way
        URLSession.shared.dataTask(with: request) {
            (data,response, error) in
            //convert data to string to print to consol
            do{
                let dataString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                print(dataString!)
                
                guard let data = data else{
                    throw JSONError.NoData
                }
                
                // by here we have data so lets serialize it
                // we use array of dictionary cus each colum in a octionary
                guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? [NSDictionary]
                    else{
                        throw JSONError.ConversionFailed
                    }
                print(json)
                // set the ictionary to a variable that you know of
                self.dbData = json
            }catch let error as JSONError{
                print(error.rawValue)
            }catch let error as NSError{
                print(error.debugDescription)
            }
        }.resume()
    }
    
    
    
    //get data from remote sql data about highscores
    func jsonParserHighScores(){
        //url object
        guard let endpoint = URL(string: myScoresUrl) else{
            print("Error creating connection")
            return
        }
        //url request object
        let request = URLRequest(url: endpoint)
        //get data by asynce way
        URLSession.shared.dataTask(with: request) {
            (data,response, error) in
            //convert data to string to print to consol
            do{
                let dataString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                print(dataString!)
                
                guard let data = data else{
                    throw JSONError.NoData
                }
                
                // by here we have data so lets serialize it
                // we use array of dictionary cus each colum in a octionary
                guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? [NSDictionary]
                    else{
                        throw JSONError.ConversionFailed
                }
                print(json)
                // set the ictionary to a variable that you know of
                self.dbScoresData = json
            }catch let error as JSONError{
                print(error.rawValue)
            }catch let error as NSError{
                print(error.debugDescription)
            }
            }.resume()
    }
    
    
    func jsonParserExercise(){
        //url object
        guard let endpoint = URL(string: myExerciseUrl) else{
            print("Error creating connection")
            return
        }
        //url request object
        let request = URLRequest(url: endpoint)
        //get data by asynce way
        URLSession.shared.dataTask(with: request) {
            (data,response, error) in
            //convert data to string to print to consol
            do{
                let dataString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                print(dataString!)
                
                guard let data = data else{
                    throw JSONError.NoData
                }
                
                // by here we have data so lets serialize it
                // we use array of dictionary cus each colum in a octionary
                guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? [NSDictionary]
                    else{
                        throw JSONError.ConversionFailed
                }
                print(json)
                // set the ictionary to a variable that you know of
                self.dbExerciseData = json
            }catch let error as JSONError{
                print(error.rawValue)
            }catch let error as NSError{
                print(error.debugDescription)
            }
            }.resume()
    }
    
    func jsonParserRoutines(){
        //url object
        guard let endpoint = URL(string: myRoutinesUrl) else{
            print("Error creating connection")
            return
        }
        //url request object
        let request = URLRequest(url: endpoint)
        //get data by asynce way
        URLSession.shared.dataTask(with: request) {
            (data,response, error) in
            //convert data to string to print to consol
            do{
                let dataString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                print(dataString!)
                
                guard let data = data else{
                    throw JSONError.NoData
                }
                
                // by here we have data so lets serialize it
                // we use array of dictionary cus each colum in a octionary
                guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? [NSDictionary]
                    else{
                        throw JSONError.ConversionFailed
                }
                print(json)
                // set the ictionary to a variable that you know of
                self.dbRoutinesData = json
            }catch let error as JSONError{
                print(error.rawValue)
            }catch let error as NSError{
                print(error.debugDescription)
            }
            }.resume()
    }
    
    func jsonParserSession(){
        //url object
        guard let endpoint = URL(string: mySessionUrl) else{
            print("Error creating connection")
            return
        }
        //url request object
        let request = URLRequest(url: endpoint)
        //get data by asynce way
        URLSession.shared.dataTask(with: request) {
            (data,response, error) in
            //convert data to string to print to consol
            do{
                let dataString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                print(dataString!)
                
                guard let data = data else{
                    throw JSONError.NoData
                }
                
                // by here we have data so lets serialize it
                // we use array of dictionary cus each colum in a octionary
                guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? [NSDictionary]
                    else{
                        throw JSONError.ConversionFailed
                }
                print(json)
                // set the ictionary to a variable that you know of
                self.dbSessionData = json
            }catch let error as JSONError{
                print(error.rawValue)
            }catch let error as NSError{
                print(error.debugDescription)
            }
            }.resume()
    }
   
}
