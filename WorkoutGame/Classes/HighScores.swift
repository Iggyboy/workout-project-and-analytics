//
//  HighScores.swift
//  WorkoutGame
//
//  Created by Igbinosa Idahosa on 11/18/18.
//  Copyright © 2018 Igbinosa Idahosa. All rights reserved.
//

import UIKit
//represents the Scores in the remote sql database
class HighScores: NSObject {

    var highScore : Int?
    var date : String?
}
