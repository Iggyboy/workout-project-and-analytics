//
//  Routine.swift
//  GameAppInTvOS
//
//  Created by Igbinosa Idahosa on 12/13/18.
//  Copyright © 2018 Igbinosa Idahosa. All rights reserved.
//

import UIKit
//represents the Routine in the remote sql database
class Routine: NSObject {

    var routines_id : Int?
    var routine_name : String?
}
