//
//  Session.swift
//  GameAppInTvOS
//
//  Created by Igbinosa Idahosa on 12/13/18.
//  Copyright © 2018 Igbinosa Idahosa. All rights reserved.
//

import UIKit
//represents the session in the remote sql database
class Session: NSObject {

    var session_id : Int?
    var routines_id : Int?
    var session_date : String?
}
