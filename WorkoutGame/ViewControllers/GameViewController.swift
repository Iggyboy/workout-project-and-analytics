//
//  GameViewController.swift
//  WorkoutGame
//
//  Created by Igbinosa Idahosa on 2018-11-12.
//  Copyright © 2018 Igbinosa Idahosa. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit
//it serves as the controller for a page view that the user interacts with in iPhone
class GameViewController: UIViewController {
    
    //lets me perform additional initialization on views that were loaded
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let view = self.view as! SKView? {
            // Load the SKScene from 'GameScene.sks'
            if let scene = SKScene(fileNamed: "MenuScene") {
                // Set the scale mode to scale to fit the window
                scene.scaleMode = .aspectFill
                
                // Present the scene
                view.presentScene(scene)
            }
            
            view.ignoresSiblingOrder = true
            
            view.showsFPS = true
            view.showsNodeCount = true
        }
    }

    override var shouldAutorotate: Bool {
        return true
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }
    //this method is called when the system determines that the amount of available memory is low
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
}
